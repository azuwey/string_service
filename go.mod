module gitlab.com/bloom-flare/string_service

require (
	github.com/golang/protobuf v1.2.0
	github.com/kpango/glg v1.2.1
	github.com/urfave/cli v1.20.0
	golang.org/x/net v0.0.0-20181017193950-04a2e542c03f
	google.golang.org/grpc v1.15.0
)
