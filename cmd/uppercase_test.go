package cmd

import (
	"context"
	"testing"

	api "gitlab.com/bloom-flare/string_service/api"
)

type TestCaseUppercase struct {
	input    string
	expected string
}

func TestUppercaseValid(t *testing.T) {
	var testCase = TestCaseUppercase{
		input:    "test",
		expected: "TEST",
	}

	s := StringServiceServer{}

	req := &api.UppercaseRequest{
		Input: testCase.input,
	}

	resp, err := s.Uppercase(context.Background(), req)

	if err != nil {
		t.Errorf("Uppercase test got unexpected error %s", err)
	}

	if resp.Message != testCase.expected {
		t.Errorf("TestUppercaseValid have %s, wanted %v", resp.Message, testCase.expected)
	}
}

func TestUppercaseInvalid(t *testing.T) {
	var testCase = TestCaseUppercase{
		input:    "",
		expected: "Empty string",
	}

	s := StringServiceServer{}

	req := &api.UppercaseRequest{
		Input: testCase.input,
	}

	resp, err := s.Uppercase(context.Background(), req)

	if err.Error() != testCase.expected {
		t.Errorf("TestUppercaseInvalid have %s, wanted %v", resp.Message, testCase.expected)
	}
}
