package cmd

import (
	"context"

	api "gitlab.com/bloom-flare/string_service/api"
)

// Count function returns the length of the string..
func (s *StringServiceServer) Count(ctx context.Context, request *api.CountRequest) (*api.CountResponse, error) {
	return &api.CountResponse{
		Count: int32(len(request.Input)),
	}, nil
}
